<h1 align="center">
    Daily Planner
</h1>

<p align="center">
    <a href="https://gitlab.com/bakunovdo/daily-planner/-/commits/master">
        <img alt="pipeline status" src="https://gitlab.com/bakunovdo/daily-planner/badges/master/pipeline.svg" />
    </a>
    <a href="https://gitlab.com/bakunovdo/daily-planner/-/commits/master">
        <img alt="coverage report" src="https://gitlab.com/bakunovdo/daily-planner/badges/master/coverage.svg" />
    </a>
</p>
 
**Daily Planner** mobile app, integration via Google Calendar API 

## ✨ Design 
- [Main Template](https://dribbble.com/shots/6091755-Get-Organized-Daily-Planner)
- [Secondary Template](https://dribbble.com/shots/9350002-Planner-and-Reminder-App-Exploration)

## 📖 Documentation
- [Google Calendar](https://developers.google.com/calendar/api/guides/overview)
