module.exports = {
  '@assets': './src/assets',
  '@shared': './src/shared',
  '@features': './src/features',
  '@navigators': './src/navigators',
  '@screens': './src/screens',
  '@entities': './src/entities',
};
